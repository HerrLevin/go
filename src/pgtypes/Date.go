package pgtypes

import (
	"cloud.google.com/go/civil"
	"database/sql"
	"database/sql/driver"
	"fmt"
)

type Date struct {
	civil.Date
}

var _ sql.Scanner = (*Date)(nil)

func (dt *Date) Scan(src interface{}) (err error) {
	switch src := src.(type) {
	case civil.Date:
		*dt = Date{src}
		return nil
	case string:
		dt.Date, err = civil.ParseDate(src)
		return err
	case []byte:
		dt.Date, err = civil.ParseDate(string(src))
		return err
	case nil:
		dt.Date = civil.Date{}
		return nil
	default:
		return fmt.Errorf("unsupported data type: %T", src)
	}
}

var _ driver.Valuer = (*Date)(nil)

func (dt Date) Value() (driver.Value, error) {
	return dt.Date.String(), nil
}
