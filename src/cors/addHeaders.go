package cors

import (
	"net/http"
	"regexp"
)

func AddHeaders(w http.ResponseWriter, r *http.Request) {
	if r.Header["Origin"] != nil {
		if match, _ := regexp.MatchString("^http://localhost(:4200)?$", r.Header["Origin"][0]); match {
			w.Header().Set("Access-Control-Allow-Origin", r.Header["Origin"][0])
			w.Header().Set("Vary", "Origin")
		}
		w.Header().Set("Access-Control-Allow-Headers", "Authorization")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	}
}
